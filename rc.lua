require("awful")
require("awful.autofocus")
require("awful.rules")
require("beautiful")
require("naughty")
require("vicious")
require("debian.menu")

awehome = awful.util.getdir("config") .. "/"
terminal = "sakura"
editor = os.getenv("EDITOR") or "vim"
editor_cmd = terminal .. " -e " .. editor
modkey = "Mod4"
browser = "firefox"

-- тема
--beautiful.init(awehome .. "qliphoth/theme.lua")
beautiful.init("/usr/share/awesome/themes/zenburn/theme.lua")

layouts = {
            awful.layout.suit.floating, --1
            awful.layout.suit.tile, --2 
            awful.layout.suit.max, --3
            awful.layout.suit.max.fullscreen, --4
			awful.layout.suit.tile.left,
			awful.layout.suit.tile.top,
			awful.layout.suit.spiral
          }

tags = {}
tags[1] = awful.tag({ "τ", "ρ", "ω", "ι", "μ", "α", "β", "γ", "δ" }, 1, layouts[3])
awful.layout.set(layouts[2], tags[1][1])
awful.layout.set(layouts[2], tags[1][9])
awful.layout.set(layouts[2], tags[1][6])
awful.layout.set(layouts[2], tags[1][8])

naughty.config.default_preset.position         = "bottom_right"
naughty.config.default_preset.margin           = 4
naughty.config.default_preset.gap              = 1
naughty.config.default_preset.border_width     = 1

-- МЕНЮ {{{
mymainmenu = awful.menu({ items = { 
                                    { "terminal", terminal },
                                   	{ "Debian", debian.menu.Debian_menu.Debian },
								    { "restart", awesome.restart },
                                    { "quit", awesome.quit }
                                  }
                        , width = 200})
mylauncher = awful.widget.launcher({ image = image(beautiful.awesome_icon), menu = mymainmenu })
--- МЕНЮ }}}

-----------------------------
-- ВИДЖЕТЫ ------------------
-----------------------------
-- Keyboard layout widget
kbdwidget = widget({type = "textbox", name = "kbdwidget"})
--kbdwidget.border_width = 1
kbdwidget.border_color = beautiful.fg_normal
kbdwidget.text = "Eng"

function toggle_layout()
    awful.util.spawn_with_shell("dbus-send --dest=ru.gentoo.KbddService /ru/gentoo/KbddService ru.gentoo.kbdd.prev_layout")
end


kbdwidget:buttons(awful.util.table.join(
    awful.button({ }, 1, function() toggle_layout() end)
))

dbus.request_name("session", "ru.gentoo.kbdd")
dbus.add_match("session", "interface='ru.gentoo.kbdd', member='layoutChanged'")
dbus.add_signal("ru.gentoo.kbdd", function(...)
    local data = {...}
    local layout = data[2]
    lts = {[0] = "Eng", [1] = "Рус"}
    kbdwidget.text = lts[layout]
    end
)

-- Splitter (разделитель)
sp = widget({ type = "textbox" })
sp.text = "|"

-- Батарея
batwidget = widget ({ type = "textbox" })

function batInfo()
 	local f = io.popen( "acpi -b" )
 	local battery = f:read( "*all" )
	f:close()
	local status = string.match(battery, "([%d]+)%%")
	if status ~= nil and tonumber(status) <= 3 and string.match(battery, "Charging") == nil then
    	naughty.notify({ text = "I want to eat!", timeout = 0, hover_timeout = 0.5 })
	end
	batwidget.text = status .. "%"
end
btimer = timer({timeout = 10})
btimer:add_signal("timeout", function() batInfo() end)
btimer:start()

-- Громкость
volwidget = widget({ type = "textbox" })
function volInfo()
    local f = io.popen("amixer get Master")
    local mixer = f:read("*all")
    f:close()
    local volu, mute = string.match(mixer, "([%d]+)%%.*%[([%l]*)")
    if volu == nil then
       volu = "0"
       mute = "off"
    end 
    if mute == "off" then
		volwidget.text = "mute"
    else
		volwidget.text = volu .. "%"
    end
end

volInfo()

volwidget:buttons(awful.util.table.join(
    awful.button({ }, 1, function() awful.util.spawn("amixer -q set Master toggle") volInfo() end),
    awful.button({ }, 3, function() awful.util.spawn(terminal .. " -e alsamixer") end),
    awful.button({ }, 4, function() awful.util.spawn("amixer set Master 2dB+") volInfo() end),
    awful.button({ }, 5, function() awful.util.spawn("amixer set Master 2dB-") volInfo() end)
))

-- Часы
mytextclock = awful.widget.textclock({ align = "right"}, "%d %H:%M")
local calendar = nil
local offset = 0
function remove_calendar()
    if calendar ~= nil then
        naughty.destroy(calendar)
        calendar = nil
        offset = 0
    end
end
function add_calendar(inc_offset)
    local save_offset = offset
    remove_calendar()
    offset = save_offset + inc_offset
    local datespec = os.date("*t")
    datespec = datespec.year * 12 + datespec.month - 1 + offset
    datespec = (datespec % 12 + 1) .. " " .. math.floor(datespec / 12)
    local cal = awful.util.pread("cal -h -m " .. datespec)
    --cal = string.gsub(cal, "^%s*(.-)%s*$", "%1")
    calendar = naughty.notify({
        text = string.format(os.date("%a, %d %B %Y") .. "\n\n" .. cal),
        timeout = 0, hover_timeout = 0.5
    })
end
mytextclock:buttons(awful.util.table.join(
    awful.button({ }, 1, function() add_calendar(0) end),
    awful.button({ }, 3, function() remove_calendar() end),
    awful.button({ }, 4, function() add_calendar(-1) end),
    awful.button({ }, 5, function() add_calendar(1) end)
))

 
-- Трэй
mysystray = widget({ type = "systray" })

-- Тэги
mytaglist = awful.widget.taglist(1,
                awful.widget.taglist.label.all,
                awful.util.table.join(
                             awful.button({ }, 1, awful.tag.viewonly),
                             awful.button({ modkey }, 1, awful.client.movetotag),
                             awful.button({ }, 3, awful.tag.viewtoggle),
                             awful.button({ modkey }, 3, awful.client.toggletag),
                             awful.button({ }, 4, awful.tag.viewnext),
                             awful.button({ }, 5, awful.tag.viewprev)))

-- виджет "панель задач"
mytasklist = {}
mytasklist.buttons = awful.util.table.join(
                     awful.button({ }, 1, function (c)
                                              if not c:isvisible() then
                                                  awful.tag.viewonly(c:tags()[1])
                                              end
                                              client.focus = c
                                              c:raise()
                                          end),
                     awful.button({ }, 3, function ()
                                              if instance then
                                                  instance:hide()
                                                  instance = nil
                                              else
                                                  instance = awful.menu.clients({ width=250 })
                                              end
                                          end),
                     awful.button({ }, 4, function ()
                                              awful.client.focus.byidx(1)
                                              if client.focus then client.focus:raise() end
                                          end),
                     awful.button({ }, 5, function ()
                                              awful.client.focus.byidx(-1)
                                              if client.focus then client.focus:raise() end
                                          end))
mytasklist = awful.widget.tasklist(function(c)
                                              return awful.widget.tasklist.label.currenttags(c, 1)
                                          end, mytasklist.buttons)

-- виджет для ввода
mypromptbox = awful.widget.prompt({ layout = awful.widget.layout.horizontal.leftright })

-- Виджет для переключения (отображения) лэйаута
mylayoutbox = awful.widget.layoutbox(1)
mylayoutbox:buttons(awful.util.table.join(
                       awful.button({ }, 1, function () awful.layout.inc(layouts, 1) end),
                       awful.button({ }, 3, function () awful.layout.inc(layouts, -1) end),
                       awful.button({ }, 4, function () awful.layout.inc(layouts, 1) end),
                       awful.button({ }, 5, function () awful.layout.inc(layouts, -1) end)))


---------------------------
-- ПАНЕЛИ------------------
---------------------------
mytb = awful.wibox({ position = "top", screen = 1 })

-- верхняя панель
mytb.widgets = {
    {
        -- виджеты верхней панели (слева-направо)
		mylauncher,
		mytaglist,
		mypromptbox,
        layout = awful.widget.layout.horizontal.leftright,
    },
    -- виджеты верхней панели (справа-налево)
    mylayoutbox,
	mytextclock,
	sp, batwidget, sp,
	volwidget,
	sp, kbdwidget, sp,
	mysystray,
    mytasklist,
    s == 1 and mysystray or nil,
    layout = awful.widget.layout.horizontal.rightleft,
}



---------------------------------
-- КНОПКИ МЫШИ НА РАБОЧЕМ СТОЛЕ--
---------------------------------

root.buttons(awful.util.table.join(
    awful.button({ }, 3, function () mymainmenu:toggle() end),
    awful.button({ }, 4, awful.tag.viewnext),
    awful.button({ }, 5, awful.tag.viewprev)
))

---------------------------------
-- КЛАВИАТУРА--------------------
---------------------------------

globalkeys = awful.util.table.join(
    awful.key({ modkey,           }, "Left",   awful.tag.viewprev       ),
    awful.key({ modkey,           }, "Right",  awful.tag.viewnext       ),
    awful.key({ modkey,           }, "Escape", awful.tag.history.restore),

    awful.key({ modkey,           }, "j",
        function ()
            awful.client.focus.byidx( 1)
            if client.focus then client.focus:raise() end
        end),
    awful.key({ modkey,           }, "k",
        function ()
            awful.client.focus.byidx(-1)
            if client.focus then client.focus:raise() end
        end),
    awful.key({ modkey,           }, "w", function () mymainmenu:show(true)        end),

    -- Layout manipulation
    awful.key({ modkey, "Shift"   }, "j", function () awful.client.swap.byidx(  1)    end),
    awful.key({ modkey, "Shift"   }, "k", function () awful.client.swap.byidx( -1)    end),
    awful.key({ modkey, "Control" }, "j", function () awful.screen.focus_relative( 1) end),
    awful.key({ modkey, "Control" }, "k", function () awful.screen.focus_relative(-1) end),
    awful.key({ modkey,           }, "u", awful.client.urgent.jumpto),
    awful.key({ modkey,           }, "Tab", function ()
	awful.menu.menu_keys.down = { "Down", "Alt_L" }
		local cmenu = awful.menu.clients({width=245}, { keygrabber=true, coords={x=525, y=330} })
	end),
    awful.key({ "Mod1",           }, "Tab",
        function ()
            awful.client.focus.history.previous()
            if client.focus then
                client.focus:raise()
            end
	end),

    awful.key({ modkey,           }, "Return", function () awful.util.spawn(terminal) end),
    awful.key({ modkey,           }, "F1", function () awful.util.spawn(terminal .. " -pe tabbed") end),
    awful.key({ modkey,           }, "F2", function () awful.util.spawn("uzbl-browser") end),
    awful.key({ modkey,           }, "F3", function () awful.util.spawn("firefox") end),
    awful.key({ modkey,           }, "F4", function () awful.util.spawn("opera") end),
    awful.key({ modkey,           }, "F5", function () awful.util.spawn("deadbeef") end),
	
	awful.key({ modkey,			  }, "s",  function () awful.util.spawn("slock") end),

    awful.key({"" }, "XF86AudioRaiseVolume", function () awful.util.spawn("amixer set Master 2dB+") end),
    awful.key({"" }, "XF86AudioLowerVolume", function () awful.util.spawn("amixer set Master 2dB-") end),
    awful.key({"" }, "XF86AudioMute", function () awful.util.spawn("amixer -q set Master toggle") end),

     awful.key({modkey,  }, "XF86AudioPlay", function () awful.util.spawn("deadbeef --play") end),
     awful.key({"" }, "XF86AudioPlay", function () awful.util.spawn("deadbeef --pause") end),
     awful.key({"" }, "XF86AudioStop", function () awful.util.spawn("deadbeef --stop") end),
     awful.key({"" }, "XF86AudioPrev", function () awful.util.spawn("deadbeef --prev") end),
     awful.key({"" }, "XF86AudioNext", function () awful.util.spawn("deadbeef --next") end),

    awful.key({ modkey, "Control" }, "r", awesome.restart),
    awful.key({ modkey, "Shift"   }, "q", awesome.quit),

    awful.key({ modkey,           }, "l",     function () awful.tag.incmwfact( 0.05)    end),
    awful.key({ modkey,           }, "h",     function () awful.tag.incmwfact(-0.05)    end),
    awful.key({ modkey, "Shift"   }, "h",     function () awful.tag.incnmaster( 1)      end),
    awful.key({ modkey, "Shift"   }, "l",     function () awful.tag.incnmaster(-1)      end),
    awful.key({ modkey, "Control" }, "h",     function () awful.tag.incncol( 1)         end),
    awful.key({ modkey, "Control" }, "l",     function () awful.tag.incncol(-1)         end),
    awful.key({ modkey,           }, "space", function () awful.layout.inc(layouts,  1) end),
    awful.key({ modkey, "Shift"   }, "space", function () awful.layout.inc(layouts, -1) end),
    awful.key({ modkey,           }, "q", function ()rodentbane.start() end),
	
	-- Translating selection
    awful.key({ modkey,           }, "t", function () awful.util.spawn("mue") end),

    -- Prompt
    awful.key({ modkey },            "r",     function () mypromptbox:run() end),
    awful.key({ Mod1 },            "F2",     function () mypromptbox:run() end),
    --awful.key({ modkey },            "r",     function () awful.util.spawn("dmenu_run") end),
    --awful.key({"Mod1"},          "F2",     function () awful.util.spawn("dmenu_run") end),

    awful.key({ modkey }, "x",
              function ()
                  awful.prompt.run({ prompt = "Run Lua code: " },
                  mypromptbox.widget,
                  awful.util.eval, nil,
                  awful.util.getdir("cache") .. "/history_eval")
              end)
    
)

clientkeys = awful.util.table.join(
    awful.key({ modkey,           }, "f",      function (c) c.fullscreen = not c.fullscreen  end),
    awful.key({ modkey,			  }, "c",      function (c) c:kill()                         end),
    awful.key({ modkey, "Control" }, "space",  awful.client.floating.toggle                     ),
    awful.key({ modkey, "Control" }, "Return", function (c) c:swap(awful.client.getmaster()) end),
    awful.key({ modkey,           }, "o",      awful.client.movetoscreen                        ),
    awful.key({ modkey, "Shift"   }, "r",      function (c) c:redraw()                       end),
    awful.key({ modkey,           }, "n",      function (c) c.minimized = not c.minimized    end),
    awful.key({ modkey,           }, "m",
        function (c)
            c.maximized_horizontal = not c.maximized_horizontal
            c.maximized_vertical   = not c.maximized_vertical
        end)

)

-- Compute the maximum number of digit we need, limited to 9
keynumber = 0
for s = 1, screen.count() do
   keynumber = math.min(9, math.max(#tags[s], keynumber));
end

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it works on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, keynumber do
    globalkeys = awful.util.table.join(globalkeys,
        awful.key({ modkey }, "#" .. i + 9,
                  function ()
                        local screen = mouse.screen
                        if tags[screen][i] then
                            awful.tag.viewonly(tags[screen][i])
                        end
                  end),
        awful.key({ modkey, "Control" }, "#" .. i + 9,
                  function ()
                      local screen = mouse.screen
                      if tags[screen][i] then
                          awful.tag.viewtoggle(tags[screen][i])
                      end
                  end),
        awful.key({ modkey, "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus and tags[client.focus.screen][i] then
                          awful.client.movetotag(tags[client.focus.screen][i])
                      end
                  end),
        awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus and tags[client.focus.screen][i] then
                          awful.client.toggletag(tags[client.focus.screen][i])
                      end
                  end))
end

clientbuttons = awful.util.table.join(
    awful.button({ }, 1, function (c) client.focus = c; c:raise() end),
    awful.button({ modkey }, 1, awful.mouse.client.move),
    awful.button({ modkey }, 3, awful.mouse.client.resize))

-- Set keys
root.keys(globalkeys)

-----------------------------------
-- Правила для окошек--------------
-----------------------------------
 
awful.rules.rules = {
    -- Правило для всех окон (далее можно переопределить для конкретных)
    { rule = { },
      properties = { border_width = beautiful.border_width,
                     border_color = beautiful.border_normal,
                     focus = true,
                     keys = clientkeys,
                     buttons = clientbuttons,
                     tag = tags[1][1],
					 size_hints_honor = false } }, -- для терминала
    -- Правила для разных приложений
    { rule = { class = "Sakura" },		properties = { tag = tags[1][1] } },
    { rule = { class = "Gvim" },		properties = { tag = tags[1][2] } },
    { rule = { class = "Emacs" },		properties = { tag = tags[1][2] } },
    { rule = { name = "LibreOffice" },		properties = { tag = tags[1][2] } },
	{ rule = { class = "libreoffice-calc" },	properties = { tag = tags[1][2] } },
    { rule = { class = "libreoffice-base" },	properties = { tag = tags[1][2] } },
    { rule = { class = "libreoffice-writer" },	properties = { tag = tags[1][2] } },
    { rule = { class = "libreoffice-startcenter" },		properties = { tag = tags[1][2] } },
    { rule = { class = "Geany" },		properties = { tag = tags[1][2] } },
    { rule = { class = "Zathura" },		properties = { tag = tags[1][2] } },
    { rule = { class = "Evince" },		properties = { tag = tags[1][2] } },
    { rule = { class = "Fbreader" },		properties = { tag = tags[1][2] } },
	{ rule = { class = "Cr3" },			properties = { tag = tags[1][2] } },
    { rule = { role = "browser" },		properties = { tag = tags[1][3] } },
	{ rule = { class = "Firefox-bin" },	properties = { tag = tags[1][3] } },
	{ rule = { class = "Iceweasel" },	properties = { tag = tags[1][3] } },
	{ rule = { class = "Chromium" },	properties = { tag = tags[1][3] } },
	{ rule = { class = "Dia" },	properties = { tag = tags[1][3] } },
	{ rule = { class = "Midori" },		properties = { tag = tags[1][3] } },
	{ rule = { class = "Opera" },		properties = { tag = tags[1][3] } },
	{ rule = { class = "feh" },			properties = { tag = tags[1][4] } },
	{ rule = { class = "Shotwell" },    properties = { tag = tags[1][4] } },
	{ rule = { role = "Surf" },    properties = { tag = tags[1][4] } },
	{ rule = { class = "Vlc" },			properties = { tag = tags[1][5] } },
	{ rule = { class = "<unknown>" },	properties = { tag = tags[1][5] } },
    { rule = { class = "Deadbeef" },	properties = { tag = tags[1][5] } },
    { rule = { class = "Gimp" },		properties = { tag = tags[1][6] } },
    { rule = { class = "Freecad" },		properties = { tag = tags[1][6] } },
    { rule = { class = "VirtualBox" },		properties = { tag = tags[1][6] } },
    { rule = { role = "gimp-toolbox" },	properties = { floating = false } },
	{ rule = { class = "Linuxdcpp" },	properties = { tag = tags[1][7] } },
	{ rule = { class = "Xchat" },	properties = { tag = tags[1][7] } },
	{ rule = { class = "Workrave" },		properties = { tag = tags[1][8] } },
    { rule = { class = "Synaptic" },	properties = { tag = tags[1][8] } },
	{ rule = { class = "Transmission" },properties = { tag = tags[1][8] } },
	{ rule = { class = "Inkscape" },properties = { tag = tags[1][8] } },
	{ rule = { class = "Pidgin" },		properties = { tag = tags[1][9] } },
	{ rule = { class = "Gajim.py" },		properties = { tag = tags[1][9] } },
	{ rule = { class = "Instantbird" },		properties = { tag = tags[1][9] } }
}

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.add_signal("manage", function (c, startup)

    if not startup then
        -- Set the windows at the slave,
        -- i.e. put it at the end of others instead of setting it master.
        -- awful.client.setslave(c)

        -- Put windows in a smart way, only if they does not set an initial position.
        if not c.size_hints.user_position and not c.size_hints.program_position then
            awful.placement.no_overlap(c)
            awful.placement.no_offscreen(c)
        end
    end
end)

client.add_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.add_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)
